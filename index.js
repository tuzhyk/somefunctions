const l = (...items) => items.join("\n");

toString = list => {
  if (list) {
    return list[list.indexOf("\n") + 1]
      ? `(${list.split("\n").join(", ")})`
      : `(${list.split("\n").join("")})`;
  }
  return "()";
};

const head = list => {
  if (list.indexOf("\n") === -1) return list.substring(0);
  return list.substring(0, list.indexOf("\n"));
};

const tail = list => {
  if (list.indexOf("\n") === -1) return undefined;
  return list.substring(list.indexOf("\n") + 1);
};

const isEmpty = list => {
  if (list[0]) return false;
  return true;
};

const cons = (item, list) => {
  let res = item;
  res += "\n" + list;
  return res;
};

const filter = (func, list) => {
  return reduce(
    (item, acc) => {
      if (func(item)) {
        return acc ? `${acc}, ${item}` : item;
      }
      return acc;
    },
    "",
    list
  );
};

const map = (func, list) => {
  return reduce(
    (item, acc) => (acc ? `${acc}, ${func(item)}` : func(item)),
    "",
    list
  );
};

const reduce = (func, init, list) => {
  let acc = init;
  if (tail(list) !== undefined) {
    acc = func(head(list), acc);
    return reduce(func, acc, tail(list));
  }
  return (acc = func(head(list), acc));
};

module.exports = {
  l,
  head,
  tail,
  filter,
  map,
  reduce,
  isEmpty,
  cons,
  toString
};
